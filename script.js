/* Объявляем переменную numberOfFilms глобально, чтобы работать с ней внутри функции */
let numberOfFilms;

// 1. Функция для проверки введенных данных при ответе на 1-ый вопрос
function start(){
    numberOfFilms = +prompt('Сколько фильмов вы уже посмотрели?');
    /* Проверка: на пустое поле для ввода ИЛИ на кнопку Отмена ИЛИ 
       на НЕ число, то снова задаем вопрос... */
    while(numberOfFilms == '' || numberOfFilms == null || isNaN(numberOfFilms)){
        numberOfFilms = +prompt('Сколько фильмов вы уже посмотрели?');
    } 
    console.log(numberOfFilms);
}


// --------- Главный объект программы ---------
const personalMovieDB = {
    count: numberOfFilms,
    movies: {},
    actors: {},
    genres: [],
    private: false
};

// 2. Функция заносит в главный объект программы информацию о фильмах
function rememberMyFilms(){
    for (let i = 0; i < 2; i++) {
        const a = prompt(`Один из последних просмотренных фильмов ${i + 1}`),
              b = prompt('На сколько оцените его?');   
        /* Проверка на пустое поле для ввода ИЛИ на кнопку Отмена ИЛИ на количество символов */
        if(a != '' && b != '' && a != null && b != null && a.length < 50) {
            personalMovieDB.movies[a] = b; 
            console.log('done');
        }
        else{
            console.log('error');
            i--; 
        }
    }
}


// 3. Функция анализа уровня Киномании
function detectPersonalLevel(){
    if (personalMovieDB.count < 10) {
        console.log('Просмотрено довольно мало фильмов');
    } else if (personalMovieDB.count > 10 && personalMovieDB.count < 30) {
        console.log('Вы классический зритель');
    } else if (personalMovieDB.count > 30) {
        console.log('Вы киноман');
    } else {
        console.log('Произошла ошибка');
    }
}


// 4. Функция Жанров - 3-я часть задания
function writeYourGenres(){
    for(let i = 1; i <= 3; i++){
        personalMovieDB.genres[i - 1] = prompt(`Ваш любимый жанр номер ${i}`);
    }
}


// 5. Функция проверки приватности БД - 2-я часть задания
function showMyDB(hidden){
    if(!hidden){
        console.log(personalMovieDB); // Выводим главный объект программы
    }
}
